#!/bin/bash

APP_NAME="RUOYI_APP1"
JAR_FILE_NAME="ruoyi-admin.jar"
RUN_DATE=`date +'%Y%m%d_%H%M%S'`
JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.8.10-0.el8_2.x86_64
JAVA_OPTIONS="-Xms256m -Xmx1024m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=512m -DAPP_NAME=${APP_NAME}"

#echo "OS_USER="${OS_USER}
echo "RUN_DATE="${RUN_DATE}
echo "JAVA_HOME="${JAVA_HOME}
echo "JAVA_OPTIONS="${JAVA_OPTIONS}

#count=`ps -ef |grep ${OS_USER}|grep java|grep ${APP_NAME}|grep -v grep|wc -l`
count=`ps -ef |grep java|grep ${APP_NAME}|grep -v grep|wc -l`
echo "count="${count}

if [ ${count} -lt 1 ]; then
#----
#nohuplog="logs/nohup_${APP_NAME}_${RUN_DATE}.log"
nohuplog="logs/nohup_${APP_NAME}.log"
echo "  Running Now"
nohup ${JAVA_HOME}/bin/java -jar ${JAVA_OPTIONS} ${JAR_FILE_NAME} >$nohuplog 2>&1 &
#nohup ${JAVA_HOME}/bin/java -jar ${JAVA_OPTIONS} ${JAR_FILE_NAME} >/dev/null 2>&1 &
#${JAVA_HOME}/bin/java -jar ${JAVA_OPTIONS} ${JAR_FILE_NAME}
sleep 10
echo "  Running Completed"
else
echo "------------------  Running Stop, Because there is exist "${APP_NAME}"------------------------------------"
#----
fi
