#!/bin/bash

APP_NAME="RUOYI_APP1$1"

echo "APP_NAME="${APP_NAME}

tpid=`ps -ef |grep java|grep ${APP_NAME}|grep -v grep|awk '{print $2}'`
echo "tpid="${tpid}
if [ ${tpid} ]; then
    echo 'Kill Process! '${tpid}
    kill -9 $tpid
	sleep 3
else
    echo 'there is not Process can be killed!'
fi