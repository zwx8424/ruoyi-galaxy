package common.utils;

import com.ruoyi.common.utils.JavaMailUtils;
import org.junit.Test;

public class JavaMailUtilsTest {

    @Test
    public void sendEmail() {
        int res = JavaMailUtils.sendEmail(new String[]{
                        "zwx8424@163.com" //这里就是一系列的收件人的邮箱了
                },
                new String[]{
                        "zwxcc1999@163.com"//这里就是一系列的抄送人的邮箱了
                }, null, "测试邮件", "祝你快乐,这是一封测试邮件,祝您生活愉快!",
                new String[]{
                        "D:\\temp\\附件A.txt",//附件
                        "D:\\temp\\附件B.txt"
                }, JavaMailUtils.EMAIL_CONTENT_TYPE__HTML);
        System.out.println("\n发送结果:" + res);
    }
}