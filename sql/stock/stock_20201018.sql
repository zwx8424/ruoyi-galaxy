-- ----------------------------
-- Table structure for t_table_backup_config
-- ----------------------------
DROP TABLE IF EXISTS `t_table_backup_config`;
CREATE TABLE `t_table_backup_config` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `origin_table` varchar(30) NOT NULL COMMENT '来源表名',
  `target_table` varchar(30) NOT NULL COMMENT '目标表名',
  `status` tinyint DEFAULT '0' COMMENT '是否启用(0-正常、1-停用)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='表备份配置表';

-- ----------------------------
-- Table structure for t_stock_basic
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_basic`;
CREATE TABLE `t_stock_basic`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ts_code` varchar(10) NOT NULL COMMENT 'TS代码',
  `symbol` varchar(10) NOT NULL COMMENT '股票代码',
  `name` varchar(40) DEFAULT NULL COMMENT '股票名称',
  `area` varchar(40) DEFAULT NULL COMMENT '所在地域',
  `industry` varchar(40) DEFAULT NULL COMMENT '所属行业',
  `fullname` varchar(100) DEFAULT NULL COMMENT '股票全称',
  `enname` varchar(100) DEFAULT NULL COMMENT '英文全称',
  `market` varchar(10) DEFAULT NULL COMMENT '市场类型(主板/中小板/创业板)',
  `exchange` varchar(4) DEFAULT NULL COMMENT '交易所代码,SSE上交所 SZSE深交所 HKEX港交所(未上线)',
  `curr_type` varchar(4) DEFAULT NULL COMMENT '交易货币',
  `list_status` varchar(2) DEFAULT NULL COMMENT '上市状态： L上市 D退市 P暂停上市',
  `list_date` varchar(10) DEFAULT NULL COMMENT '上市日期',
  `delist_date` varchar(10) DEFAULT NULL COMMENT '退市日期',
  `is_hs` varchar(2) DEFAULT NULL COMMENT '是否沪深港通标的，N否 H沪股通 S深股通',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uni_stk_basic_ts`(`ts_code`),
  UNIQUE INDEX `uni_stk_basic_symbol`(`symbol`)
) ENGINE = InnoDB comment = '股票代码基表';

-- ----------------------------
-- Table structure for t_stock_daily_market
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_daily_market`;
CREATE TABLE `t_stock_daily_market`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ts_code` varchar(10) NOT NULL COMMENT 'TS代码',
  `trade_date` int NOT NULL COMMENT '交易日期',
  `open_price` decimal(10, 2) DEFAULT NULL COMMENT '开盘价',
  `high_price` decimal(10, 2) DEFAULT NULL COMMENT '最高价',
  `low_price` decimal(10, 2) DEFAULT NULL COMMENT '最低价',
  `close_price` decimal(10, 2) DEFAULT NULL COMMENT '收盘价',
  `pre_close` decimal(10, 2) DEFAULT NULL COMMENT '昨收价',
  `amt_change` decimal(10, 2) DEFAULT NULL COMMENT '涨跌额',
  `pct_chg` decimal(10, 2) DEFAULT NULL COMMENT '涨跌幅(未复权)',
  `vol` decimal(10, 2) DEFAULT NULL COMMENT '成交量(手)',
  `amount` decimal(12, 2) DEFAULT NULL COMMENT '成交额(千元)',
  `stk_div` decimal(10, 2) DEFAULT NULL COMMENT '每股送转',
  `cash_div_tax` decimal(10, 2) DEFAULT NULL COMMENT '每股分红(税前)',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_stk_daily_mkt_ts_tr`(`ts_code`, `trade_date`),
  INDEX `idx_stk_daily_mkt_tr`(`trade_date`)
) ENGINE = InnoDB comment = '股票每日行情数据';

-- ----------------------------
-- Table structure for t_stock_daily_quota
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_daily_quota`;
CREATE TABLE `t_stock_daily_quota`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ts_code` varchar(10) NOT NULL COMMENT 'TS代码',
  `trade_date` int NOT NULL COMMENT '交易日期',
  `close_price` decimal(10, 2) DEFAULT NULL COMMENT '当日收盘价',
  `turnover_rate` decimal(10, 2) DEFAULT NULL COMMENT '换手率(%)',
  `turnover_rate_f` decimal(10, 2) DEFAULT NULL COMMENT '换手率(自由流通股)',
  `volume_ratio` decimal(10, 2) DEFAULT NULL COMMENT '量比',
  `pe` decimal(10, 2) DEFAULT NULL COMMENT '市盈率(总市值/净利润，亏损的PE为空)',
  `pe_ttm` decimal(10, 2) DEFAULT NULL COMMENT '市盈率(TTM，亏损的PE为空)',
  `pb` decimal(10, 2) DEFAULT NULL COMMENT '市净率(总市值/净资产)',
  `ps` decimal(10, 2) DEFAULT NULL COMMENT '市销率',
  `ps_ttm` decimal(10, 2) DEFAULT NULL COMMENT '市销率(TTM)',
  `dv_ratio` decimal(10, 2) DEFAULT NULL COMMENT '股息率(%)',
  `dv_ttm` decimal(10, 2) DEFAULT NULL COMMENT '股息率(TTM)(%)',
  `total_share` decimal(12, 2) DEFAULT NULL COMMENT '总股本(万股)',
  `float_share` decimal(12, 2) DEFAULT NULL COMMENT '流通股本(万股)',
  `free_share` decimal(12, 2) DEFAULT NULL COMMENT '自由流通股本(万股)',
  `total_mv` decimal(12, 2) DEFAULT NULL COMMENT '总市值(万元)',
  `circ_mv` decimal(12, 2) DEFAULT NULL COMMENT '流通市值(万元)',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_stk_daily_qta_ts_tr`(`ts_code`, `trade_date`),
  INDEX `idx_stk_daily_qta_tr`(`trade_date`)
) ENGINE = InnoDB comment = '股票每日指标';

-- ----------------------------
-- Table structure for t_stock_fin_income
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_fin_income`;
CREATE TABLE `t_stock_fin_income`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ts_code` varchar(10) NOT NULL COMMENT 'TS代码',
  `ann_date` int NOT NULL COMMENT '公告日期',
  `f_ann_date` int NOT NULL COMMENT '实际公告日期',
  `end_date` int DEFAULT NULL COMMENT '报告期',
  `report_type` tinyint DEFAULT NULL COMMENT '报告类型',
  `comp_type` tinyint DEFAULT NULL COMMENT '公司类型(1一般工商业2银行3保险4证券)',
  `basic_eps` decimal(12, 2) DEFAULT NULL COMMENT '基本每股收益',
  `diluted_eps` decimal(16, 2) DEFAULT NULL COMMENT '稀释每股收益',
  `total_revenue` decimal(16, 2) DEFAULT NULL COMMENT '营业总收入',
  `revenue` decimal(16, 2) DEFAULT NULL COMMENT '营业收入',
  `int_income` decimal(16, 2) DEFAULT NULL COMMENT '利息收入',
  `prem_earned` decimal(16, 2) DEFAULT NULL COMMENT '已赚保费',
  `comm_income` decimal(16, 2) DEFAULT NULL COMMENT '手续费及佣金收入',
  `n_commis_income` decimal(16, 2) DEFAULT NULL COMMENT '手续费及佣金净收入',
  `n_oth_income` decimal(16, 2) DEFAULT NULL COMMENT '其他经营净收益',
  `n_oth_b_income` decimal(16, 2) DEFAULT NULL COMMENT '加:其他业务净收益',
  `prem_income` decimal(16, 2) DEFAULT NULL COMMENT '保险业务收入',
  `out_prem` decimal(16, 2) DEFAULT NULL COMMENT '减:分出保费',
  `une_prem_reser` decimal(16, 2) DEFAULT NULL COMMENT '提取未到期责任准备金',
  `reins_income` decimal(16, 2) DEFAULT NULL COMMENT '其中:分保费收入',
  `n_sec_tb_income` decimal(16, 2) DEFAULT NULL COMMENT '代理买卖证券业务净收入',
  `n_sec_uw_income` decimal(16, 2) DEFAULT NULL COMMENT '证券承销业务净收入',
  `n_asset_mg_income` decimal(16, 2) DEFAULT NULL COMMENT '受托客户资产管理业务净收入',
  `oth_b_income` decimal(16, 2) DEFAULT NULL COMMENT '其他业务收入',
  `fv_value_chg_gain` decimal(16, 2) DEFAULT NULL COMMENT '加:公允价值变动净收益',
  `invest_income` decimal(16, 2) DEFAULT NULL COMMENT '加:投资净收益',
  `ass_invest_income` decimal(16, 2) DEFAULT NULL COMMENT '其中:对联营企业和合营企业的投资收益',
  `forex_gain` decimal(16, 2) DEFAULT NULL COMMENT '加:汇兑净收益',
  `total_cogs` decimal(16, 2) DEFAULT NULL COMMENT '营业总成本',
  `oper_cost` decimal(16, 2) DEFAULT NULL COMMENT '减:营业成本',
  `int_exp` decimal(16, 2) DEFAULT NULL COMMENT '减:利息支出',
  `comm_exp` decimal(16, 2) DEFAULT NULL COMMENT '减:手续费及佣金支出',
  `biz_tax_surchg` decimal(16, 2) DEFAULT NULL COMMENT '减:营业税金及附加',
  `sell_exp` decimal(16, 2) DEFAULT NULL COMMENT '减:销售费用',
  `admin_exp` decimal(16, 2) DEFAULT NULL COMMENT '减:管理费用',
  `fin_exp` decimal(16, 2) DEFAULT NULL COMMENT '减:财务费用',
  `assets_impair_loss` decimal(16, 2) DEFAULT NULL COMMENT '减:资产减值损失',
  `prem_refund` decimal(16, 2) DEFAULT NULL COMMENT '退保金',
  `compens_payout` decimal(16, 2) DEFAULT NULL COMMENT '赔付总支出',
  `reser_insur_liab` decimal(16, 2) DEFAULT NULL COMMENT '提取保险责任准备金',
  `div_payt` decimal(16, 2) DEFAULT NULL COMMENT '保户红利支出',
  `reins_exp` decimal(16, 2) DEFAULT NULL COMMENT '分保费用',
  `oper_exp` decimal(16, 2) DEFAULT NULL COMMENT '营业支出',
  `compens_payout_refu` decimal(16, 2) DEFAULT NULL COMMENT '减:摊回赔付支出',
  `insur_reser_refu` decimal(16, 2) DEFAULT NULL COMMENT '减:摊回保险责任准备金',
  `reins_cost_refund` decimal(16, 2) DEFAULT NULL COMMENT '减:摊回分保费用',
  `other_bus_cost` decimal(16, 2) DEFAULT NULL COMMENT '其他业务成本',
  `operate_profit` decimal(16, 2) DEFAULT NULL COMMENT '营业利润',
  `non_oper_income` decimal(16, 2) DEFAULT NULL COMMENT '加:营业外收入',
  `non_oper_exp` decimal(16, 2) DEFAULT NULL COMMENT '减:营业外支出',
  `nca_disploss` decimal(16, 2) DEFAULT NULL COMMENT '其中:减:非流动资产处置净损失',
  `total_profit` decimal(16, 2) DEFAULT NULL COMMENT '利润总额',
  `income_tax` decimal(16, 2) DEFAULT NULL COMMENT '所得税费用',
  `n_income` decimal(16, 2) DEFAULT NULL COMMENT '净利润(含少数股东损益)',
  `n_income_attr_p` decimal(16, 2) DEFAULT NULL COMMENT '净利润(不含少数股东损益)',
  `minority_gain` decimal(16, 2) DEFAULT NULL COMMENT '少数股东损益',
  `oth_compr_income` decimal(16, 2) DEFAULT NULL COMMENT '其他综合收益',
  `t_compr_income` decimal(16, 2) DEFAULT NULL COMMENT '综合收益总额',
  `compr_inc_attr_p` decimal(16, 2) DEFAULT NULL COMMENT '归属于母公司(或股东)的综合收益总额',
  `compr_inc_attr_m_s` decimal(16, 2) DEFAULT NULL COMMENT '归属于少数股东的综合收益总额',
  `ebit` decimal(16, 2) DEFAULT NULL COMMENT '息税前利润',
  `ebitda` decimal(16, 2) DEFAULT NULL COMMENT '息税折旧摊销前利润',
  `insurance_exp` decimal(16, 2) DEFAULT NULL COMMENT '保险业务支出',
  `undist_profit` decimal(16, 2) DEFAULT NULL COMMENT '年初未分配利润',
  `distable_profit` decimal(16, 2) DEFAULT NULL COMMENT '可分配利润',
  `update_flag` tinyint DEFAULT NULL COMMENT '更新标识，0未修改1更正过',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_stk_fin_inc_ts`(`ts_code`, `end_date`),
  INDEX `idx_stk_fin_inc_en`(`end_date`)
) ENGINE = InnoDB comment = '股票财务利润表';