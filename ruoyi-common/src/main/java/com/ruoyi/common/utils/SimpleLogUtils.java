 /*
  * Copyright (C), weixin.zhu
  * FileName: SimpleLogUtil
  * Author:   weixin.zhu
  * Date:     2021/2/26 10:18
  * Description: 简单日志工具
  * History:
  * <author>          <time>          <version>          <desc>
  * 作者姓名           修改时间           版本号              描述
  */
 package com.ruoyi.common.utils;

 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 import org.slf4j.event.Level;

 /**
  * 用于简单场景，不想定义日志对象的静态变量时，且不关注日志位置的可读性时<br>
  * 〈简单日志工具〉
  *
  * @author weixin.zhu
  * @create 2021/2/26
  */
 public class SimpleLogUtils {

     private static final Logger log = LoggerFactory.getLogger(SimpleLogUtils.class);

     public static void print(String msg, Level level) {
         switch (level) {
             case INFO:
                 log.info(msg);
                 break;
             case DEBUG:
                 log.debug(msg);
                 break;
             case ERROR:
                 log.error(msg);
                 break;
             case WARN:
                 log.warn(msg);
                 break;
             case TRACE:
                 log.trace(msg);
                 break;
         }

     }

     public static void print(String msg) {
         log.info(msg);
     }

 }