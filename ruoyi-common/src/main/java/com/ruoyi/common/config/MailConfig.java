 /*
  * Copyright (C), weixin.zhu
  * FileName: MailConfig
  * Author:   weixin.zhu
  * Date:     2021/2/26 9:08
  * Description: 邮件发送配置类
  * History:
  * <author>          <time>          <version>          <desc>
  * 作者姓名           修改时间           版本号              描述
  */
 package com.ruoyi.common.config;

 import com.ruoyi.common.utils.ExceptionUtil;
 import com.ruoyi.common.utils.StringUtils;
 import org.yaml.snakeyaml.Yaml;

 import java.io.File;
 import java.io.FileInputStream;
 import java.io.FileNotFoundException;
 import java.io.InputStream;
 import java.util.LinkedHashMap;
 import java.util.Map;

 /**
  * 〈一句话功能简述〉<br>
  * 〈邮件发送配置类〉
  *
  * @author weixin.zhu
  * @create 2021/2/26
  */
 public class MailConfig {
     private static final String MAIL_CFG_FILE_PATH = "application-mail.yml";
     /**
      * 发送邮件开关
      */
     private static boolean switchTurnOn;
     /**
      * 服务器地址
      */
     private static String host;
     /**
      * 发信箱邮箱账号
      */
     private static String username;

     /**
      * 发信箱邮箱密码
      */
     private static String password;
     /**
      * 发信箱邮箱端口
      */
     private static Integer port;
     /**
      * 发信箱邮箱编码
      */
     private static boolean debugTurnOn;

     private static Map<?, ?> loadCfgFile(String filePath) {
         try {
             return loadYaml(filePath);
         } catch (Exception e) {
             System.err.println("load email configuration file failed: " + filePath);
             //e.printStackTrace();
             return null;
         }
     }

     static {
         switchTurnOn = false;
         debugTurnOn = false;

         Map<?, ?> map;
         String cfgFilePath;

         // 1.获取与jar平行目录的config目录下的配置文件
         cfgFilePath = System.getProperty("user.dir") + File.separator + "config" + File.separator + MAIL_CFG_FILE_PATH;
         map = loadCfgFile(cfgFilePath);

         // 2.获取与jar平行目录下的配置文件
         if (map == null) {
             cfgFilePath = System.getProperty("user.dir") + File.separator + MAIL_CFG_FILE_PATH;
             map = loadCfgFile(cfgFilePath);
         }

         // 3.获取classpath/config目录下配置文件
         if (map == null) {
             cfgFilePath = "config" + File.separator + MAIL_CFG_FILE_PATH;
             map = loadCfgFile(cfgFilePath);
         }

         // 4.获取classpath下配置文件
         if (map == null) {
             cfgFilePath = MAIL_CFG_FILE_PATH;
             map = loadCfgFile(cfgFilePath);
         }
         if (map != null) {
             try {
                 switchTurnOn = "Y".equals(String.valueOf(getProperty(map, "mail.switchFlag")));
                 host = (String) getProperty(map, "mail.host");
                 username = (String) getProperty(map, "mail.username");
                 password = (String) getProperty(map, "mail.password");
                 port = (Integer) getProperty(map, "mail.port");
                 debugTurnOn = "Y".equals(String.valueOf(getProperty(map, "mail.debugFlag")));

                 if (debugTurnOn) {
                     System.out.println(map.toString());
                     System.out.println("switchTurnOn:" + switchTurnOn);
                     System.out.println("host:" + host);
                     System.out.println("username:" + username);
                     System.out.println("password:" + password);
                     System.out.println("port:" + port);
                     System.out.println("debugTurnOn:" + debugTurnOn);
                     System.out.println("--host is null:" + (host == null));
                     System.out.println("--port is null:" + (port == null));
                 }

             } catch (Exception e) {
                 switchTurnOn = false;
                 System.err.println("load email parameters fail! configuration file is:" + cfgFilePath);
                 System.err.println(ExceptionUtil.getExceptionMessage(e));
             }
         }

     }

     public static Map<?, ?> loadYaml(String fileName) throws FileNotFoundException {
         //InputStream in = MailConfig.class.getClassLoader().getResourceAsStream(fileName);
         InputStream in = new FileInputStream(fileName);
         return StringUtils.isNotEmpty(fileName) ? (LinkedHashMap<?, ?>) new Yaml().load(in) : null;
     }


     public static Object getProperty(Map<?, ?> map, Object qualifiedKey) {
         if (map != null && !map.isEmpty() && qualifiedKey != null) {
             String input = String.valueOf(qualifiedKey);
             if (!"".equals(input)) {
                 if (input.contains(".")) {
                     int index = input.indexOf(".");
                     String left = input.substring(0, index);
                     String right = input.substring(index + 1);
                     return getProperty((Map<?, ?>) map.get(left), right);
                 } else if (map.containsKey(input)) {
                     return map.get(input);
                 } else {
                     return null;
                 }
             }
         }
         return null;
     }

     public static boolean isSwitchTurnOn() {
         return switchTurnOn;
     }

     public static String getHost() {
         return host;
     }

     public static String getUsername() {
         return username;
     }

     public static String getPassword() {
         return password;
     }

     public static Integer getPort() {
         return port;
     }

     public static boolean isDebugTurnOn() {
         return debugTurnOn;
     }
 }