package com.ruoyi.common.annotation;

import java.lang.annotation.*;

/**
 * 自定义遇到Error发邮件给管理源
 *
 * @author weixin
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SendEmailWhenException {
    /**
     * 内容描述
     */
    String title() default "";
}
