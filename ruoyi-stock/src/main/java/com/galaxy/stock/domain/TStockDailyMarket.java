package com.galaxy.stock.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 股票每日行情数据对象 t_stock_daily_market
 * 
 * @author lucifer
 * @date 2020-09-12
 */
public class TStockDailyMarket extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** TS代码 */
    @Excel(name = "TS代码")
    private String tsCode;

    /** 交易日期 */
    @Excel(name = "交易日期")
    private Long tradeDate;

    /** 开盘价 */
    @Excel(name = "开盘价")
    private BigDecimal openPrice;

    /** 最高价 */
    @Excel(name = "最高价")
    private BigDecimal highPrice;

    /** 最低价 */
    @Excel(name = "最低价")
    private BigDecimal lowPrice;

    /** 收盘价 */
    @Excel(name = "收盘价")
    private BigDecimal closePrice;

    /** 昨收价 */
    @Excel(name = "昨收价")
    private BigDecimal preClose;

    /** 涨跌额 */
    @Excel(name = "涨跌额")
    private BigDecimal amtChange;

    /** 涨跌幅<br/>(未复权) */
    @Excel(name = "涨跌幅<br/>(未复权)")
    private BigDecimal pctChg;

    /** 成交量<br/>(手) */
    @Excel(name = "成交量<br/>(手)")
    private BigDecimal vol;

    /** 成交额<br/>(千元) */
    @Excel(name = "成交额<br/>(千元)")
    private BigDecimal amount;

    /** 每股送转 */
    @Excel(name = "每股送转")
    private BigDecimal stkDiv;

    /** 每股分红<br/>(税前) */
    @Excel(name = "每股分红<br/>(税前)")
    private BigDecimal cashDivTax;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTsCode(String tsCode) 
    {
        this.tsCode = tsCode;
    }

    public String getTsCode() 
    {
        return tsCode;
    }
    public void setTradeDate(Long tradeDate) 
    {
        this.tradeDate = tradeDate;
    }

    public Long getTradeDate() 
    {
        return tradeDate;
    }
    public void setOpenPrice(BigDecimal openPrice) 
    {
        this.openPrice = openPrice;
    }

    public BigDecimal getOpenPrice() 
    {
        return openPrice;
    }
    public void setHighPrice(BigDecimal highPrice) 
    {
        this.highPrice = highPrice;
    }

    public BigDecimal getHighPrice() 
    {
        return highPrice;
    }
    public void setLowPrice(BigDecimal lowPrice) 
    {
        this.lowPrice = lowPrice;
    }

    public BigDecimal getLowPrice() 
    {
        return lowPrice;
    }
    public void setClosePrice(BigDecimal closePrice) 
    {
        this.closePrice = closePrice;
    }

    public BigDecimal getClosePrice() 
    {
        return closePrice;
    }
    public void setPreClose(BigDecimal preClose) 
    {
        this.preClose = preClose;
    }

    public BigDecimal getPreClose() 
    {
        return preClose;
    }
    public void setAmtChange(BigDecimal amtChange) 
    {
        this.amtChange = amtChange;
    }

    public BigDecimal getAmtChange() 
    {
        return amtChange;
    }
    public void setPctChg(BigDecimal pctChg) 
    {
        this.pctChg = pctChg;
    }

    public BigDecimal getPctChg() 
    {
        return pctChg;
    }
    public void setVol(BigDecimal vol) 
    {
        this.vol = vol;
    }

    public BigDecimal getVol() 
    {
        return vol;
    }
    public void setAmount(BigDecimal amount) 
    {
        this.amount = amount;
    }

    public BigDecimal getAmount() 
    {
        return amount;
    }
    public void setStkDiv(BigDecimal stkDiv) 
    {
        this.stkDiv = stkDiv;
    }

    public BigDecimal getStkDiv() 
    {
        return stkDiv;
    }
    public void setCashDivTax(BigDecimal cashDivTax) 
    {
        this.cashDivTax = cashDivTax;
    }

    public BigDecimal getCashDivTax() 
    {
        return cashDivTax;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("tsCode", getTsCode())
            .append("tradeDate", getTradeDate())
            .append("openPrice", getOpenPrice())
            .append("highPrice", getHighPrice())
            .append("lowPrice", getLowPrice())
            .append("closePrice", getClosePrice())
            .append("preClose", getPreClose())
            .append("amtChange", getAmtChange())
            .append("pctChg", getPctChg())
            .append("vol", getVol())
            .append("amount", getAmount())
            .append("stkDiv", getStkDiv())
            .append("cashDivTax", getCashDivTax())
            .toString();
    }
}
