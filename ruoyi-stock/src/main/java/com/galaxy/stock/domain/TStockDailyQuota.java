package com.galaxy.stock.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 股票每日指标对象 t_stock_daily_quota
 * 
 * @author lucifer
 * @date 2020-09-12
 */
public class TStockDailyQuota extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** TS代码 */
    @Excel(name = "TS代码")
    private String tsCode;

    /** 交易日期 */
    @Excel(name = "交易日期")
    private Long tradeDate;

    /** 收盘价 */
    @Excel(name = "收盘价")
    private BigDecimal closePrice;

    /** 换手率 */
    @Excel(name = "换手率")
    private BigDecimal turnoverRate;

    /** 换手率<br/>(自由流通股) */
    @Excel(name = "换手率<br/>(自由流通股)")
    private BigDecimal turnoverRateF;

    /** 量比 */
    @Excel(name = "量比")
    private BigDecimal volumeRatio;

    /** 市盈率 */
    @Excel(name = "市盈率")
    private BigDecimal pe;

    /** 市盈率(TTM) */
    @Excel(name = "市盈率(TTM)")
    private BigDecimal peTtm;

    /** 市净率 */
    @Excel(name = "市净率")
    private BigDecimal pb;

    /** 市销率 */
    @Excel(name = "市销率")
    private BigDecimal ps;

    /** 市销率<br/>(TTM) */
    @Excel(name = "市销率<br/>(TTM)")
    private BigDecimal psTtm;

    /** 股息率 */
    @Excel(name = "股息率")
    private BigDecimal dvRatio;

    /** 股息率<br/>(TTM) */
    @Excel(name = "股息率<br/>(TTM)")
    private BigDecimal dvTtm;

    /** 总股本<br/>(万股) */
    @Excel(name = "总股本<br/>(万股)")
    private BigDecimal totalShare;

    /** 流通股本<br/>(万股) */
    @Excel(name = "流通股本<br/>(万股)")
    private BigDecimal floatShare;

    /** 自由流通股本<br/>(万股) */
    @Excel(name = "自由流通股本<br/>(万股)")
    private BigDecimal freeShare;

    /** 总市值<br/>(万元) */
    @Excel(name = "总市值<br/>(万元)")
    private BigDecimal totalMv;

    /** 流通市值<br/>(万元) */
    @Excel(name = "流通市值<br/>(万元)")
    private BigDecimal circMv;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTsCode(String tsCode) 
    {
        this.tsCode = tsCode;
    }

    public String getTsCode() 
    {
        return tsCode;
    }
    public void setTradeDate(Long tradeDate) 
    {
        this.tradeDate = tradeDate;
    }

    public Long getTradeDate() 
    {
        return tradeDate;
    }
    public void setClosePrice(BigDecimal closePrice) 
    {
        this.closePrice = closePrice;
    }

    public BigDecimal getClosePrice() 
    {
        return closePrice;
    }
    public void setTurnoverRate(BigDecimal turnoverRate) 
    {
        this.turnoverRate = turnoverRate;
    }

    public BigDecimal getTurnoverRate() 
    {
        return turnoverRate;
    }
    public void setTurnoverRateF(BigDecimal turnoverRateF) 
    {
        this.turnoverRateF = turnoverRateF;
    }

    public BigDecimal getTurnoverRateF() 
    {
        return turnoverRateF;
    }
    public void setVolumeRatio(BigDecimal volumeRatio) 
    {
        this.volumeRatio = volumeRatio;
    }

    public BigDecimal getVolumeRatio() 
    {
        return volumeRatio;
    }
    public void setPe(BigDecimal pe) 
    {
        this.pe = pe;
    }

    public BigDecimal getPe() 
    {
        return pe;
    }
    public void setPeTtm(BigDecimal peTtm) 
    {
        this.peTtm = peTtm;
    }

    public BigDecimal getPeTtm() 
    {
        return peTtm;
    }
    public void setPb(BigDecimal pb) 
    {
        this.pb = pb;
    }

    public BigDecimal getPb() 
    {
        return pb;
    }
    public void setPs(BigDecimal ps) 
    {
        this.ps = ps;
    }

    public BigDecimal getPs() 
    {
        return ps;
    }
    public void setPsTtm(BigDecimal psTtm) 
    {
        this.psTtm = psTtm;
    }

    public BigDecimal getPsTtm() 
    {
        return psTtm;
    }
    public void setDvRatio(BigDecimal dvRatio) 
    {
        this.dvRatio = dvRatio;
    }

    public BigDecimal getDvRatio() 
    {
        return dvRatio;
    }
    public void setDvTtm(BigDecimal dvTtm) 
    {
        this.dvTtm = dvTtm;
    }

    public BigDecimal getDvTtm() 
    {
        return dvTtm;
    }
    public void setTotalShare(BigDecimal totalShare) 
    {
        this.totalShare = totalShare;
    }

    public BigDecimal getTotalShare() 
    {
        return totalShare;
    }
    public void setFloatShare(BigDecimal floatShare) 
    {
        this.floatShare = floatShare;
    }

    public BigDecimal getFloatShare() 
    {
        return floatShare;
    }
    public void setFreeShare(BigDecimal freeShare) 
    {
        this.freeShare = freeShare;
    }

    public BigDecimal getFreeShare() 
    {
        return freeShare;
    }
    public void setTotalMv(BigDecimal totalMv) 
    {
        this.totalMv = totalMv;
    }

    public BigDecimal getTotalMv() 
    {
        return totalMv;
    }
    public void setCircMv(BigDecimal circMv) 
    {
        this.circMv = circMv;
    }

    public BigDecimal getCircMv() 
    {
        return circMv;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("tsCode", getTsCode())
            .append("tradeDate", getTradeDate())
            .append("closePrice", getClosePrice())
            .append("turnoverRate", getTurnoverRate())
            .append("turnoverRateF", getTurnoverRateF())
            .append("volumeRatio", getVolumeRatio())
            .append("pe", getPe())
            .append("peTtm", getPeTtm())
            .append("pb", getPb())
            .append("ps", getPs())
            .append("psTtm", getPsTtm())
            .append("dvRatio", getDvRatio())
            .append("dvTtm", getDvTtm())
            .append("totalShare", getTotalShare())
            .append("floatShare", getFloatShare())
            .append("freeShare", getFreeShare())
            .append("totalMv", getTotalMv())
            .append("circMv", getCircMv())
            .toString();
    }
}
