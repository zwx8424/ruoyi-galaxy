package com.galaxy.stock.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 股票代码对象 t_stock_basic
 * 
 * @author lucifer
 * @date 2020-09-12
 */
public class TStockBasic extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** TS代码 */
    @Excel(name = "TS代码")
    private String tsCode;

    /** 股票代码 */
    @Excel(name = "股票代码")
    private String symbol;

    /** 股票名称 */
    @Excel(name = "股票名称")
    private String name;

    /** 所在<br/>地域 */
    @Excel(name = "所在<br/>地域")
    private String area;

    /** 所属行业 */
    @Excel(name = "所属行业")
    private String industry;

    /** 股票全称 */
    @Excel(name = "股票全称")
    private String fullname;

    /** 英文全称 */
    private String enname;

    /** 市场类型 */
    @Excel(name = "市场类型")
    private String market;

    /** 交易所<br/>代码 */
    @Excel(name = "交易所<br/>代码")
    private String exchange;

    /** 交易<br/>货币 */
    @Excel(name = "交易<br/>货币")
    private String currType;

    /** 上市<br/>状态 */
    @Excel(name = "上市<br/>状态")
    private String listStatus;

    /** 上市日期 */
    @Excel(name = "上市日期")
    private String listDate;

    /** 退市日期 */
    @Excel(name = "退市日期")
    private String delistDate;

    /** 沪深港通<br/>标的 */
    @Excel(name = "沪深港通<br/>标的")
    private String isHs;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTsCode(String tsCode) 
    {
        this.tsCode = tsCode;
    }

    public String getTsCode() 
    {
        return tsCode;
    }
    public void setSymbol(String symbol) 
    {
        this.symbol = symbol;
    }

    public String getSymbol() 
    {
        return symbol;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setArea(String area) 
    {
        this.area = area;
    }

    public String getArea() 
    {
        return area;
    }
    public void setIndustry(String industry) 
    {
        this.industry = industry;
    }

    public String getIndustry() 
    {
        return industry;
    }
    public void setFullname(String fullname) 
    {
        this.fullname = fullname;
    }

    public String getFullname() 
    {
        return fullname;
    }
    public void setEnname(String enname) 
    {
        this.enname = enname;
    }

    public String getEnname() 
    {
        return enname;
    }
    public void setMarket(String market) 
    {
        this.market = market;
    }

    public String getMarket() 
    {
        return market;
    }
    public void setExchange(String exchange) 
    {
        this.exchange = exchange;
    }

    public String getExchange() 
    {
        return exchange;
    }
    public void setCurrType(String currType) 
    {
        this.currType = currType;
    }

    public String getCurrType() 
    {
        return currType;
    }
    public void setListStatus(String listStatus) 
    {
        this.listStatus = listStatus;
    }

    public String getListStatus() 
    {
        return listStatus;
    }
    public void setListDate(String listDate) 
    {
        this.listDate = listDate;
    }

    public String getListDate() 
    {
        return listDate;
    }
    public void setDelistDate(String delistDate) 
    {
        this.delistDate = delistDate;
    }

    public String getDelistDate() 
    {
        return delistDate;
    }
    public void setIsHs(String isHs) 
    {
        this.isHs = isHs;
    }

    public String getIsHs() 
    {
        return isHs;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("tsCode", getTsCode())
            .append("symbol", getSymbol())
            .append("name", getName())
            .append("area", getArea())
            .append("industry", getIndustry())
            .append("fullname", getFullname())
            .append("enname", getEnname())
            .append("market", getMarket())
            .append("exchange", getExchange())
            .append("currType", getCurrType())
            .append("listStatus", getListStatus())
            .append("listDate", getListDate())
            .append("delistDate", getDelistDate())
            .append("isHs", getIsHs())
            .toString();
    }
}
