package com.galaxy.stock.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 备份配置对象 t_table_backup_config
 * 
 * @author lucifer
 * @date 2020-10-18
 */
public class TTableBackupConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 来源表名 */
    @Excel(name = "来源表名")
    private String originTable;

    /** 目标表名 */
    @Excel(name = "目标表名")
    private String targetTable;

    /** 是否启用<br/>(0-正常、1-停用) */
    @Excel(name = "是否启用<br/>(0-正常、1-停用)")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOriginTable(String originTable) 
    {
        this.originTable = originTable;
    }

    public String getOriginTable() 
    {
        return originTable;
    }
    public void setTargetTable(String targetTable) 
    {
        this.targetTable = targetTable;
    }

    public String getTargetTable() 
    {
        return targetTable;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("originTable", getOriginTable())
            .append("targetTable", getTargetTable())
            .append("status", getStatus())
            .toString();
    }
}
