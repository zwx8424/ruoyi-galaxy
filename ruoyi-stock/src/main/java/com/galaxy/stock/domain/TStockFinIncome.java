package com.galaxy.stock.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 股票财务利润表对象 t_stock_fin_income
 * 
 * @author lucifer
 * @date 2020-09-12
 */
public class TStockFinIncome extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** TS代码 */
    @Excel(name = "TS代码")
    private String tsCode;

    /** 公告日期 */
    @Excel(name = "公告日期")
    private Long annDate;

    /** 实际公告日期 */
    @Excel(name = "实际公告日期")
    private Long fAnnDate;

    /** 报告期 */
    @Excel(name = "报告期")
    private Long endDate;

    /** 报告类型 */
    @Excel(name = "报告类型")
    private Integer reportType;

    /** 公司类型 */
    @Excel(name = "公司类型")
    private Integer compType;

    /** 基本每股收益 */
    @Excel(name = "基本每股收益")
    private BigDecimal basicEps;

    /** 稀释每股收益 */
    @Excel(name = "稀释每股收益")
    private BigDecimal dilutedEps;

    /** 营业总收入 */
    @Excel(name = "营业总收入")
    private BigDecimal totalRevenue;

    /** 营业收入 */
    @Excel(name = "营业收入")
    private BigDecimal revenue;

    /** 利息收入 */
    @Excel(name = "利息收入")
    private BigDecimal intIncome;

    /** 已赚保费 */
    @Excel(name = "已赚保费")
    private BigDecimal premEarned;

    /** 手续费及佣金收入 */
    @Excel(name = "手续费及佣金收入")
    private BigDecimal commIncome;

    /** 手续费及佣金净收入 */
    @Excel(name = "手续费及佣金净收入")
    private BigDecimal nCommisIncome;

    /** 其他经营净收益 */
    @Excel(name = "其他经营净收益")
    private BigDecimal nOthIncome;

    /** 加:其他业务净收益 */
    @Excel(name = "加:其他业务净收益")
    private BigDecimal nOthBIncome;

    /** 保险业务收入 */
    @Excel(name = "保险业务收入")
    private BigDecimal premIncome;

    /** 减:分出保费 */
    @Excel(name = "减:分出保费")
    private BigDecimal outPrem;

    /** 提取未到期责任准备金 */
    @Excel(name = "提取未到期责任准备金")
    private BigDecimal unePremReser;

    /** 其中:分保费收入 */
    @Excel(name = "其中:分保费收入")
    private BigDecimal reinsIncome;

    /** 代理买卖证券业务净收入 */
    @Excel(name = "代理买卖证券业务净收入")
    private BigDecimal nSecTbIncome;

    /** 证券承销业务净收入 */
    @Excel(name = "证券承销业务净收入")
    private BigDecimal nSecUwIncome;

    /** 受托客户资产管理业务净收入 */
    @Excel(name = "受托客户资产管理业务净收入")
    private BigDecimal nAssetMgIncome;

    /** 其他业务收入 */
    @Excel(name = "其他业务收入")
    private BigDecimal othBIncome;

    /** 加:公允价值变动净收益 */
    @Excel(name = "加:公允价值变动净收益")
    private BigDecimal fvValueChgGain;

    /** 加:投资净收益 */
    @Excel(name = "加:投资净收益")
    private BigDecimal investIncome;

    /** 其中:对联营企业和合营企业的投资收益 */
    @Excel(name = "其中:对联营企业和合营企业的投资收益")
    private BigDecimal assInvestIncome;

    /** 加:汇兑净收益 */
    @Excel(name = "加:汇兑净收益")
    private BigDecimal forexGain;

    /** 营业总成本 */
    @Excel(name = "营业总成本")
    private BigDecimal totalCogs;

    /** 减:营业成本 */
    @Excel(name = "减:营业成本")
    private BigDecimal operCost;

    /** 减:利息支出 */
    @Excel(name = "减:利息支出")
    private BigDecimal intExp;

    /** 减:手续费及佣金支出 */
    @Excel(name = "减:手续费及佣金支出")
    private BigDecimal commExp;

    /** 减:营业税金及附加 */
    @Excel(name = "减:营业税金及附加")
    private BigDecimal bizTaxSurchg;

    /** 减:销售费用 */
    @Excel(name = "减:销售费用")
    private BigDecimal sellExp;

    /** 减:管理费用 */
    @Excel(name = "减:管理费用")
    private BigDecimal adminExp;

    /** 减:财务费用 */
    @Excel(name = "减:财务费用")
    private BigDecimal finExp;

    /** 减:资产减值损失 */
    @Excel(name = "减:资产减值损失")
    private BigDecimal assetsImpairLoss;

    /** 退保金 */
    @Excel(name = "退保金")
    private BigDecimal premRefund;

    /** 赔付总支出 */
    @Excel(name = "赔付总支出")
    private BigDecimal compensPayout;

    /** 提取保险责任准备金 */
    @Excel(name = "提取保险责任准备金")
    private BigDecimal reserInsurLiab;

    /** 保户红利支出 */
    @Excel(name = "保户红利支出")
    private BigDecimal divPayt;

    /** 分保费用 */
    @Excel(name = "分保费用")
    private BigDecimal reinsExp;

    /** 营业支出 */
    @Excel(name = "营业支出")
    private BigDecimal operExp;

    /** 减:摊回赔付支出 */
    @Excel(name = "减:摊回赔付支出")
    private BigDecimal compensPayoutRefu;

    /** 减:摊回保险责任准备金 */
    @Excel(name = "减:摊回保险责任准备金")
    private BigDecimal insurReserRefu;

    /** 减:摊回分保费用 */
    @Excel(name = "减:摊回分保费用")
    private BigDecimal reinsCostRefund;

    /** 其他业务成本 */
    @Excel(name = "其他业务成本")
    private BigDecimal otherBusCost;

    /** 营业利润 */
    @Excel(name = "营业利润")
    private BigDecimal operateProfit;

    /** 加:营业外收入 */
    @Excel(name = "加:营业外收入")
    private BigDecimal nonOperIncome;

    /** 减:营业外支出 */
    @Excel(name = "减:营业外支出")
    private BigDecimal nonOperExp;

    /** 其中:减:非流动资产处置净损失 */
    @Excel(name = "其中:减:非流动资产处置净损失")
    private BigDecimal ncaDisploss;

    /** 利润总额 */
    @Excel(name = "利润总额")
    private BigDecimal totalProfit;

    /** 所得税费用 */
    @Excel(name = "所得税费用")
    private BigDecimal incomeTax;

    /** 净利润(含少数股东损益) */
    @Excel(name = "净利润(含少数股东损益)")
    private BigDecimal nIncome;

    /** 净利润(不含少数股东损益) */
    @Excel(name = "净利润(不含少数股东损益)")
    private BigDecimal nIncomeAttrP;

    /** 少数股东损益 */
    @Excel(name = "少数股东损益")
    private BigDecimal minorityGain;

    /** 其他综合收益 */
    @Excel(name = "其他综合收益")
    private BigDecimal othComprIncome;

    /** 综合收益总额 */
    @Excel(name = "综合收益总额")
    private BigDecimal tComprIncome;

    /** 归属于母公司(或股东)的综合收益总额 */
    @Excel(name = "归属于母公司(或股东)的综合收益总额")
    private BigDecimal comprIncAttrP;

    /** 归属于少数股东的综合收益总额 */
    @Excel(name = "归属于少数股东的综合收益总额")
    private BigDecimal comprIncAttrMS;

    /** 息税前利润 */
    @Excel(name = "息税前利润")
    private BigDecimal ebit;

    /** 息税折旧摊销前利润 */
    @Excel(name = "息税折旧摊销前利润")
    private BigDecimal ebitda;

    /** 保险业务支出 */
    @Excel(name = "保险业务支出")
    private BigDecimal insuranceExp;

    /** 年初未分配利润 */
    @Excel(name = "年初未分配利润")
    private BigDecimal undistProfit;

    /** 可分配利润 */
    @Excel(name = "可分配利润")
    private BigDecimal distableProfit;

    /** 更新标识 */
    @Excel(name = "更新标识")
    private Integer updateFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTsCode(String tsCode) 
    {
        this.tsCode = tsCode;
    }

    public String getTsCode() 
    {
        return tsCode;
    }
    public void setAnnDate(Long annDate) 
    {
        this.annDate = annDate;
    }

    public Long getAnnDate() 
    {
        return annDate;
    }
    public void setfAnnDate(Long fAnnDate) 
    {
        this.fAnnDate = fAnnDate;
    }

    public Long getfAnnDate() 
    {
        return fAnnDate;
    }
    public void setEndDate(Long endDate) 
    {
        this.endDate = endDate;
    }

    public Long getEndDate() 
    {
        return endDate;
    }
    public void setReportType(Integer reportType) 
    {
        this.reportType = reportType;
    }

    public Integer getReportType() 
    {
        return reportType;
    }
    public void setCompType(Integer compType) 
    {
        this.compType = compType;
    }

    public Integer getCompType() 
    {
        return compType;
    }
    public void setBasicEps(BigDecimal basicEps) 
    {
        this.basicEps = basicEps;
    }

    public BigDecimal getBasicEps() 
    {
        return basicEps;
    }
    public void setDilutedEps(BigDecimal dilutedEps) 
    {
        this.dilutedEps = dilutedEps;
    }

    public BigDecimal getDilutedEps() 
    {
        return dilutedEps;
    }
    public void setTotalRevenue(BigDecimal totalRevenue) 
    {
        this.totalRevenue = totalRevenue;
    }

    public BigDecimal getTotalRevenue() 
    {
        return totalRevenue;
    }
    public void setRevenue(BigDecimal revenue) 
    {
        this.revenue = revenue;
    }

    public BigDecimal getRevenue() 
    {
        return revenue;
    }
    public void setIntIncome(BigDecimal intIncome) 
    {
        this.intIncome = intIncome;
    }

    public BigDecimal getIntIncome() 
    {
        return intIncome;
    }
    public void setPremEarned(BigDecimal premEarned) 
    {
        this.premEarned = premEarned;
    }

    public BigDecimal getPremEarned() 
    {
        return premEarned;
    }
    public void setCommIncome(BigDecimal commIncome) 
    {
        this.commIncome = commIncome;
    }

    public BigDecimal getCommIncome() 
    {
        return commIncome;
    }
    public void setnCommisIncome(BigDecimal nCommisIncome) 
    {
        this.nCommisIncome = nCommisIncome;
    }

    public BigDecimal getnCommisIncome() 
    {
        return nCommisIncome;
    }
    public void setnOthIncome(BigDecimal nOthIncome) 
    {
        this.nOthIncome = nOthIncome;
    }

    public BigDecimal getnOthIncome() 
    {
        return nOthIncome;
    }
    public void setnOthBIncome(BigDecimal nOthBIncome) 
    {
        this.nOthBIncome = nOthBIncome;
    }

    public BigDecimal getnOthBIncome() 
    {
        return nOthBIncome;
    }
    public void setPremIncome(BigDecimal premIncome) 
    {
        this.premIncome = premIncome;
    }

    public BigDecimal getPremIncome() 
    {
        return premIncome;
    }
    public void setOutPrem(BigDecimal outPrem) 
    {
        this.outPrem = outPrem;
    }

    public BigDecimal getOutPrem() 
    {
        return outPrem;
    }
    public void setUnePremReser(BigDecimal unePremReser) 
    {
        this.unePremReser = unePremReser;
    }

    public BigDecimal getUnePremReser() 
    {
        return unePremReser;
    }
    public void setReinsIncome(BigDecimal reinsIncome) 
    {
        this.reinsIncome = reinsIncome;
    }

    public BigDecimal getReinsIncome() 
    {
        return reinsIncome;
    }
    public void setnSecTbIncome(BigDecimal nSecTbIncome) 
    {
        this.nSecTbIncome = nSecTbIncome;
    }

    public BigDecimal getnSecTbIncome() 
    {
        return nSecTbIncome;
    }
    public void setnSecUwIncome(BigDecimal nSecUwIncome) 
    {
        this.nSecUwIncome = nSecUwIncome;
    }

    public BigDecimal getnSecUwIncome() 
    {
        return nSecUwIncome;
    }
    public void setnAssetMgIncome(BigDecimal nAssetMgIncome) 
    {
        this.nAssetMgIncome = nAssetMgIncome;
    }

    public BigDecimal getnAssetMgIncome() 
    {
        return nAssetMgIncome;
    }
    public void setOthBIncome(BigDecimal othBIncome) 
    {
        this.othBIncome = othBIncome;
    }

    public BigDecimal getOthBIncome() 
    {
        return othBIncome;
    }
    public void setFvValueChgGain(BigDecimal fvValueChgGain) 
    {
        this.fvValueChgGain = fvValueChgGain;
    }

    public BigDecimal getFvValueChgGain() 
    {
        return fvValueChgGain;
    }
    public void setInvestIncome(BigDecimal investIncome) 
    {
        this.investIncome = investIncome;
    }

    public BigDecimal getInvestIncome() 
    {
        return investIncome;
    }
    public void setAssInvestIncome(BigDecimal assInvestIncome) 
    {
        this.assInvestIncome = assInvestIncome;
    }

    public BigDecimal getAssInvestIncome() 
    {
        return assInvestIncome;
    }
    public void setForexGain(BigDecimal forexGain) 
    {
        this.forexGain = forexGain;
    }

    public BigDecimal getForexGain() 
    {
        return forexGain;
    }
    public void setTotalCogs(BigDecimal totalCogs) 
    {
        this.totalCogs = totalCogs;
    }

    public BigDecimal getTotalCogs() 
    {
        return totalCogs;
    }
    public void setOperCost(BigDecimal operCost) 
    {
        this.operCost = operCost;
    }

    public BigDecimal getOperCost() 
    {
        return operCost;
    }
    public void setIntExp(BigDecimal intExp) 
    {
        this.intExp = intExp;
    }

    public BigDecimal getIntExp() 
    {
        return intExp;
    }
    public void setCommExp(BigDecimal commExp) 
    {
        this.commExp = commExp;
    }

    public BigDecimal getCommExp() 
    {
        return commExp;
    }
    public void setBizTaxSurchg(BigDecimal bizTaxSurchg) 
    {
        this.bizTaxSurchg = bizTaxSurchg;
    }

    public BigDecimal getBizTaxSurchg() 
    {
        return bizTaxSurchg;
    }
    public void setSellExp(BigDecimal sellExp) 
    {
        this.sellExp = sellExp;
    }

    public BigDecimal getSellExp() 
    {
        return sellExp;
    }
    public void setAdminExp(BigDecimal adminExp) 
    {
        this.adminExp = adminExp;
    }

    public BigDecimal getAdminExp() 
    {
        return adminExp;
    }
    public void setFinExp(BigDecimal finExp) 
    {
        this.finExp = finExp;
    }

    public BigDecimal getFinExp() 
    {
        return finExp;
    }
    public void setAssetsImpairLoss(BigDecimal assetsImpairLoss) 
    {
        this.assetsImpairLoss = assetsImpairLoss;
    }

    public BigDecimal getAssetsImpairLoss() 
    {
        return assetsImpairLoss;
    }
    public void setPremRefund(BigDecimal premRefund) 
    {
        this.premRefund = premRefund;
    }

    public BigDecimal getPremRefund() 
    {
        return premRefund;
    }
    public void setCompensPayout(BigDecimal compensPayout) 
    {
        this.compensPayout = compensPayout;
    }

    public BigDecimal getCompensPayout() 
    {
        return compensPayout;
    }
    public void setReserInsurLiab(BigDecimal reserInsurLiab) 
    {
        this.reserInsurLiab = reserInsurLiab;
    }

    public BigDecimal getReserInsurLiab() 
    {
        return reserInsurLiab;
    }
    public void setDivPayt(BigDecimal divPayt) 
    {
        this.divPayt = divPayt;
    }

    public BigDecimal getDivPayt() 
    {
        return divPayt;
    }
    public void setReinsExp(BigDecimal reinsExp) 
    {
        this.reinsExp = reinsExp;
    }

    public BigDecimal getReinsExp() 
    {
        return reinsExp;
    }
    public void setOperExp(BigDecimal operExp) 
    {
        this.operExp = operExp;
    }

    public BigDecimal getOperExp() 
    {
        return operExp;
    }
    public void setCompensPayoutRefu(BigDecimal compensPayoutRefu) 
    {
        this.compensPayoutRefu = compensPayoutRefu;
    }

    public BigDecimal getCompensPayoutRefu() 
    {
        return compensPayoutRefu;
    }
    public void setInsurReserRefu(BigDecimal insurReserRefu) 
    {
        this.insurReserRefu = insurReserRefu;
    }

    public BigDecimal getInsurReserRefu() 
    {
        return insurReserRefu;
    }
    public void setReinsCostRefund(BigDecimal reinsCostRefund) 
    {
        this.reinsCostRefund = reinsCostRefund;
    }

    public BigDecimal getReinsCostRefund() 
    {
        return reinsCostRefund;
    }
    public void setOtherBusCost(BigDecimal otherBusCost) 
    {
        this.otherBusCost = otherBusCost;
    }

    public BigDecimal getOtherBusCost() 
    {
        return otherBusCost;
    }
    public void setOperateProfit(BigDecimal operateProfit) 
    {
        this.operateProfit = operateProfit;
    }

    public BigDecimal getOperateProfit() 
    {
        return operateProfit;
    }
    public void setNonOperIncome(BigDecimal nonOperIncome) 
    {
        this.nonOperIncome = nonOperIncome;
    }

    public BigDecimal getNonOperIncome() 
    {
        return nonOperIncome;
    }
    public void setNonOperExp(BigDecimal nonOperExp) 
    {
        this.nonOperExp = nonOperExp;
    }

    public BigDecimal getNonOperExp() 
    {
        return nonOperExp;
    }
    public void setNcaDisploss(BigDecimal ncaDisploss) 
    {
        this.ncaDisploss = ncaDisploss;
    }

    public BigDecimal getNcaDisploss() 
    {
        return ncaDisploss;
    }
    public void setTotalProfit(BigDecimal totalProfit) 
    {
        this.totalProfit = totalProfit;
    }

    public BigDecimal getTotalProfit() 
    {
        return totalProfit;
    }
    public void setIncomeTax(BigDecimal incomeTax) 
    {
        this.incomeTax = incomeTax;
    }

    public BigDecimal getIncomeTax() 
    {
        return incomeTax;
    }
    public void setnIncome(BigDecimal nIncome) 
    {
        this.nIncome = nIncome;
    }

    public BigDecimal getnIncome() 
    {
        return nIncome;
    }
    public void setnIncomeAttrP(BigDecimal nIncomeAttrP) 
    {
        this.nIncomeAttrP = nIncomeAttrP;
    }

    public BigDecimal getnIncomeAttrP() 
    {
        return nIncomeAttrP;
    }
    public void setMinorityGain(BigDecimal minorityGain) 
    {
        this.minorityGain = minorityGain;
    }

    public BigDecimal getMinorityGain() 
    {
        return minorityGain;
    }
    public void setOthComprIncome(BigDecimal othComprIncome) 
    {
        this.othComprIncome = othComprIncome;
    }

    public BigDecimal getOthComprIncome() 
    {
        return othComprIncome;
    }
    public void settComprIncome(BigDecimal tComprIncome) 
    {
        this.tComprIncome = tComprIncome;
    }

    public BigDecimal gettComprIncome() 
    {
        return tComprIncome;
    }
    public void setComprIncAttrP(BigDecimal comprIncAttrP) 
    {
        this.comprIncAttrP = comprIncAttrP;
    }

    public BigDecimal getComprIncAttrP() 
    {
        return comprIncAttrP;
    }
    public void setComprIncAttrMS(BigDecimal comprIncAttrMS) 
    {
        this.comprIncAttrMS = comprIncAttrMS;
    }

    public BigDecimal getComprIncAttrMS() 
    {
        return comprIncAttrMS;
    }
    public void setEbit(BigDecimal ebit) 
    {
        this.ebit = ebit;
    }

    public BigDecimal getEbit() 
    {
        return ebit;
    }
    public void setEbitda(BigDecimal ebitda) 
    {
        this.ebitda = ebitda;
    }

    public BigDecimal getEbitda() 
    {
        return ebitda;
    }
    public void setInsuranceExp(BigDecimal insuranceExp) 
    {
        this.insuranceExp = insuranceExp;
    }

    public BigDecimal getInsuranceExp() 
    {
        return insuranceExp;
    }
    public void setUndistProfit(BigDecimal undistProfit) 
    {
        this.undistProfit = undistProfit;
    }

    public BigDecimal getUndistProfit() 
    {
        return undistProfit;
    }
    public void setDistableProfit(BigDecimal distableProfit) 
    {
        this.distableProfit = distableProfit;
    }

    public BigDecimal getDistableProfit() 
    {
        return distableProfit;
    }
    public void setUpdateFlag(Integer updateFlag) 
    {
        this.updateFlag = updateFlag;
    }

    public Integer getUpdateFlag() 
    {
        return updateFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("tsCode", getTsCode())
            .append("annDate", getAnnDate())
            .append("fAnnDate", getfAnnDate())
            .append("endDate", getEndDate())
            .append("reportType", getReportType())
            .append("compType", getCompType())
            .append("basicEps", getBasicEps())
            .append("dilutedEps", getDilutedEps())
            .append("totalRevenue", getTotalRevenue())
            .append("revenue", getRevenue())
            .append("intIncome", getIntIncome())
            .append("premEarned", getPremEarned())
            .append("commIncome", getCommIncome())
            .append("nCommisIncome", getnCommisIncome())
            .append("nOthIncome", getnOthIncome())
            .append("nOthBIncome", getnOthBIncome())
            .append("premIncome", getPremIncome())
            .append("outPrem", getOutPrem())
            .append("unePremReser", getUnePremReser())
            .append("reinsIncome", getReinsIncome())
            .append("nSecTbIncome", getnSecTbIncome())
            .append("nSecUwIncome", getnSecUwIncome())
            .append("nAssetMgIncome", getnAssetMgIncome())
            .append("othBIncome", getOthBIncome())
            .append("fvValueChgGain", getFvValueChgGain())
            .append("investIncome", getInvestIncome())
            .append("assInvestIncome", getAssInvestIncome())
            .append("forexGain", getForexGain())
            .append("totalCogs", getTotalCogs())
            .append("operCost", getOperCost())
            .append("intExp", getIntExp())
            .append("commExp", getCommExp())
            .append("bizTaxSurchg", getBizTaxSurchg())
            .append("sellExp", getSellExp())
            .append("adminExp", getAdminExp())
            .append("finExp", getFinExp())
            .append("assetsImpairLoss", getAssetsImpairLoss())
            .append("premRefund", getPremRefund())
            .append("compensPayout", getCompensPayout())
            .append("reserInsurLiab", getReserInsurLiab())
            .append("divPayt", getDivPayt())
            .append("reinsExp", getReinsExp())
            .append("operExp", getOperExp())
            .append("compensPayoutRefu", getCompensPayoutRefu())
            .append("insurReserRefu", getInsurReserRefu())
            .append("reinsCostRefund", getReinsCostRefund())
            .append("otherBusCost", getOtherBusCost())
            .append("operateProfit", getOperateProfit())
            .append("nonOperIncome", getNonOperIncome())
            .append("nonOperExp", getNonOperExp())
            .append("ncaDisploss", getNcaDisploss())
            .append("totalProfit", getTotalProfit())
            .append("incomeTax", getIncomeTax())
            .append("nIncome", getnIncome())
            .append("nIncomeAttrP", getnIncomeAttrP())
            .append("minorityGain", getMinorityGain())
            .append("othComprIncome", getOthComprIncome())
            .append("tComprIncome", gettComprIncome())
            .append("comprIncAttrP", getComprIncAttrP())
            .append("comprIncAttrMS", getComprIncAttrMS())
            .append("ebit", getEbit())
            .append("ebitda", getEbitda())
            .append("insuranceExp", getInsuranceExp())
            .append("undistProfit", getUndistProfit())
            .append("distableProfit", getDistableProfit())
            .append("updateFlag", getUpdateFlag())
            .toString();
    }
}
