package com.galaxy.stock.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.galaxy.stock.service.IDataBackupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数据备份任务
 * 
 * @author lucifer
 * @date 2020-10-18
 */
@Component("dataBackupTask")
public class DataBackupTask {
    private static final Logger log = LoggerFactory.getLogger(DataBackupTask.class);

    @Autowired
    private IDataBackupService dataBackupService;
    
    public void backupProcess() {
        log.info("backup start---------------------");
        dataBackupService.backup();
        log.info("backup finished ---------------------");
    }

}
