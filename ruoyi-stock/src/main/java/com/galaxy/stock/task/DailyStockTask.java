package com.galaxy.stock.task;

import com.ruoyi.common.annotation.SendEmailWhenException;
import org.springframework.stereotype.Component;
import com.galaxy.stock.utils.RuntimeExecUtils;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * stock的每日任务
 * 
 * @author lucifer
 * @date 2020-09-05
 */
@Component("fetchStockDataTask")
public class DailyStockTask {
    private static final Logger log = LoggerFactory.getLogger(DailyStockTask.class);

    @SendEmailWhenException(title = "每日自动获取当天的股票数据")
    public void dailyFetchStockData(String execFile, String pythonFile) {
        if (StringUtils.isEmpty(execFile)) {
            throw new BusinessException("execFile is empty!");
        }
        if (StringUtils.isEmpty(pythonFile)) {
            throw new BusinessException("pythonFile is empty!");
        }
        log.info("dailyFetchStockData start---------------");
        String curDay = DateUtils.dateTime();
        // "D:/Work/sourcecode/stock/bin/run.bat"
        RuntimeExecUtils.process(execFile, pythonFile, curDay, curDay);
        log.info("dailyFetchStockData finished---------------");
    }

    @SendEmailWhenException(title = "人工获取股票数据")
    public void manualFetchStockDataWithoutPeriod(String execFile, String pythonFile) {
        if (StringUtils.isEmpty(execFile)) {
            throw new BusinessException("execFile is empty!");
        }
        if (StringUtils.isEmpty(pythonFile)) {
            throw new BusinessException("pythonFile is empty!");
        }
        log.info("manualFetchStockDataWithoutPeriod start---------------");
        RuntimeExecUtils.process(execFile, pythonFile);
        log.info("manualFetchStockDataWithoutPeriod finished---------------");
    }

    @SendEmailWhenException(title = "人工获取指定日期的股票数据")
    public void manualFetchStockData(String execFile, String pythonFile, String startDate, String endDate) {
        if (StringUtils.isEmpty(execFile) || StringUtils.isEmpty(pythonFile) || StringUtils.isEmpty(startDate) || StringUtils.isEmpty(endDate)) {
            throw new BusinessException("execFile or pythonFile or startDate or endDate is empty");
        }

        log.info("fetchStockData start---------------");
        RuntimeExecUtils.process(execFile, pythonFile, startDate, endDate);
        log.info("fetchStockData finished---------------");
    }

}
