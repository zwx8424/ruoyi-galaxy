package com.galaxy.stock.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DBInfoDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<String> getDbColumnNames(String tableName) {
        String sql = "select column_name from information_schema.columns where table_schema = (select database()) and table_name = ? order by ordinal_position";
        List<String> result = jdbcTemplate.queryForList(sql, String.class, tableName); 
        return result;
    }
}
