package com.galaxy.stock.mapper;

import java.util.List;
import com.galaxy.stock.domain.TTableBackupConfig;

/**
 * 备份配置Mapper接口
 * 
 * @author lucifer
 * @date 2020-10-18
 */
public interface TTableBackupConfigMapper 
{
    /**
     * 查询备份配置
     * 
     * @param id 备份配置ID
     * @return 备份配置
     */
    public TTableBackupConfig selectTTableBackupConfigById(Long id);

    /**
     * 查询备份配置列表
     * 
     * @param tTableBackupConfig 备份配置
     * @return 备份配置集合
     */
    public List<TTableBackupConfig> selectTTableBackupConfigList(TTableBackupConfig tTableBackupConfig);

    /**
     * 新增备份配置
     * 
     * @param tTableBackupConfig 备份配置
     * @return 结果
     */
    public int insertTTableBackupConfig(TTableBackupConfig tTableBackupConfig);

    /**
     * 修改备份配置
     * 
     * @param tTableBackupConfig 备份配置
     * @return 结果
     */
    public int updateTTableBackupConfig(TTableBackupConfig tTableBackupConfig);

    /**
     * 删除备份配置
     * 
     * @param id 备份配置ID
     * @return 结果
     */
    public int deleteTTableBackupConfigById(Long id);

    /**
     * 批量删除备份配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTTableBackupConfigByIds(String[] ids);
}
