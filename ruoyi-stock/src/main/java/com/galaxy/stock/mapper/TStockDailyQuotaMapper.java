package com.galaxy.stock.mapper;

import java.util.List;
import com.galaxy.stock.domain.TStockDailyQuota;

/**
 * 股票每日指标Mapper接口
 * 
 * @author lucifer
 * @date 2020-09-12
 */
public interface TStockDailyQuotaMapper 
{
    /**
     * 查询股票每日指标
     * 
     * @param id 股票每日指标ID
     * @return 股票每日指标
     */
    public TStockDailyQuota selectTStockDailyQuotaById(Long id);

    /**
     * 查询股票每日指标列表
     * 
     * @param tStockDailyQuota 股票每日指标
     * @return 股票每日指标集合
     */
    public List<TStockDailyQuota> selectTStockDailyQuotaList(TStockDailyQuota tStockDailyQuota);

    /**
     * 新增股票每日指标
     * 
     * @param tStockDailyQuota 股票每日指标
     * @return 结果
     */
    public int insertTStockDailyQuota(TStockDailyQuota tStockDailyQuota);

    /**
     * 修改股票每日指标
     * 
     * @param tStockDailyQuota 股票每日指标
     * @return 结果
     */
    public int updateTStockDailyQuota(TStockDailyQuota tStockDailyQuota);

    /**
     * 删除股票每日指标
     * 
     * @param id 股票每日指标ID
     * @return 结果
     */
    public int deleteTStockDailyQuotaById(Long id);

    /**
     * 批量删除股票每日指标
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTStockDailyQuotaByIds(String[] ids);
}
