package com.galaxy.stock.mapper;

import java.util.List;
import com.galaxy.stock.domain.TStockFinIncome;

/**
 * 股票财务利润表Mapper接口
 * 
 * @author lucifer
 * @date 2020-09-12
 */
public interface TStockFinIncomeMapper 
{
    /**
     * 查询股票财务利润表
     * 
     * @param id 股票财务利润表ID
     * @return 股票财务利润表
     */
    public TStockFinIncome selectTStockFinIncomeById(Long id);

    /**
     * 查询股票财务利润表列表
     * 
     * @param tStockFinIncome 股票财务利润表
     * @return 股票财务利润表集合
     */
    public List<TStockFinIncome> selectTStockFinIncomeList(TStockFinIncome tStockFinIncome);

    /**
     * 新增股票财务利润表
     * 
     * @param tStockFinIncome 股票财务利润表
     * @return 结果
     */
    public int insertTStockFinIncome(TStockFinIncome tStockFinIncome);

    /**
     * 修改股票财务利润表
     * 
     * @param tStockFinIncome 股票财务利润表
     * @return 结果
     */
    public int updateTStockFinIncome(TStockFinIncome tStockFinIncome);

    /**
     * 删除股票财务利润表
     * 
     * @param id 股票财务利润表ID
     * @return 结果
     */
    public int deleteTStockFinIncomeById(Long id);

    /**
     * 批量删除股票财务利润表
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTStockFinIncomeByIds(String[] ids);
}
