package com.galaxy.stock.mapper;

import java.util.List;
import com.galaxy.stock.domain.TStockBasic;

/**
 * 股票代码Mapper接口
 * 
 * @author lucifer
 * @date 2020-09-12
 */
public interface TStockBasicMapper 
{
    /**
     * 查询股票代码
     * 
     * @param id 股票代码ID
     * @return 股票代码
     */
    public TStockBasic selectTStockBasicById(Long id);

    /**
     * 查询股票代码列表
     * 
     * @param tStockBasic 股票代码
     * @return 股票代码集合
     */
    public List<TStockBasic> selectTStockBasicList(TStockBasic tStockBasic);

    /**
     * 新增股票代码
     * 
     * @param tStockBasic 股票代码
     * @return 结果
     */
    public int insertTStockBasic(TStockBasic tStockBasic);

    /**
     * 修改股票代码
     * 
     * @param tStockBasic 股票代码
     * @return 结果
     */
    public int updateTStockBasic(TStockBasic tStockBasic);

    /**
     * 删除股票代码
     * 
     * @param id 股票代码ID
     * @return 结果
     */
    public int deleteTStockBasicById(Long id);

    /**
     * 批量删除股票代码
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTStockBasicByIds(String[] ids);
}
