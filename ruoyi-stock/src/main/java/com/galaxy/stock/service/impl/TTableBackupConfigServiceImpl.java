package com.galaxy.stock.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.galaxy.stock.mapper.TTableBackupConfigMapper;
import com.galaxy.stock.domain.TTableBackupConfig;
import com.galaxy.stock.service.ITTableBackupConfigService;
import com.ruoyi.common.core.text.Convert;

/**
 * 备份配置Service业务层处理
 * 
 * @author lucifer
 * @date 2020-10-18
 */
@Service
public class TTableBackupConfigServiceImpl implements ITTableBackupConfigService 
{
    @Autowired
    private TTableBackupConfigMapper tTableBackupConfigMapper;

    /**
     * 查询备份配置
     * 
     * @param id 备份配置ID
     * @return 备份配置
     */
    @Override
    public TTableBackupConfig selectTTableBackupConfigById(Long id)
    {
        return tTableBackupConfigMapper.selectTTableBackupConfigById(id);
    }

    /**
     * 查询备份配置列表
     * 
     * @param tTableBackupConfig 备份配置
     * @return 备份配置
     */
    @Override
    public List<TTableBackupConfig> selectTTableBackupConfigList(TTableBackupConfig tTableBackupConfig)
    {
        return tTableBackupConfigMapper.selectTTableBackupConfigList(tTableBackupConfig);
    }

    /**
     * 新增备份配置
     * 
     * @param tTableBackupConfig 备份配置
     * @return 结果
     */
    @Override
    public int insertTTableBackupConfig(TTableBackupConfig tTableBackupConfig)
    {
        return tTableBackupConfigMapper.insertTTableBackupConfig(tTableBackupConfig);
    }

    /**
     * 修改备份配置
     * 
     * @param tTableBackupConfig 备份配置
     * @return 结果
     */
    @Override
    public int updateTTableBackupConfig(TTableBackupConfig tTableBackupConfig)
    {
        return tTableBackupConfigMapper.updateTTableBackupConfig(tTableBackupConfig);
    }

    /**
     * 删除备份配置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTTableBackupConfigByIds(String ids)
    {
        return tTableBackupConfigMapper.deleteTTableBackupConfigByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除备份配置信息
     * 
     * @param id 备份配置ID
     * @return 结果
     */
    @Override
    public int deleteTTableBackupConfigById(Long id)
    {
        return tTableBackupConfigMapper.deleteTTableBackupConfigById(id);
    }
}
