package com.galaxy.stock.service;
/**
 * 数据备份Service接口
 * 
 * @author lucifer
 * @date 2020-10-18
 */
public interface IDataBackupService {
    /**
     * 备份Mysql数据表
     * 
     */
    public void backup();
}
