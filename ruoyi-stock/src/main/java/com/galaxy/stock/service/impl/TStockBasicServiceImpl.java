package com.galaxy.stock.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.galaxy.stock.mapper.TStockBasicMapper;
import com.galaxy.stock.domain.TStockBasic;
import com.galaxy.stock.service.ITStockBasicService;
import com.ruoyi.common.core.text.Convert;

/**
 * 股票代码Service业务层处理
 * 
 * @author lucifer
 * @date 2020-09-12
 */
@Service
public class TStockBasicServiceImpl implements ITStockBasicService 
{
    @Autowired
    private TStockBasicMapper tStockBasicMapper;

    /**
     * 查询股票代码
     * 
     * @param id 股票代码ID
     * @return 股票代码
     */
    @Override
    public TStockBasic selectTStockBasicById(Long id)
    {
        return tStockBasicMapper.selectTStockBasicById(id);
    }

    /**
     * 查询股票代码列表
     * 
     * @param tStockBasic 股票代码
     * @return 股票代码
     */
    @Override
    public List<TStockBasic> selectTStockBasicList(TStockBasic tStockBasic)
    {
        return tStockBasicMapper.selectTStockBasicList(tStockBasic);
    }

    /**
     * 新增股票代码
     * 
     * @param tStockBasic 股票代码
     * @return 结果
     */
    @Override
    public int insertTStockBasic(TStockBasic tStockBasic)
    {
        return tStockBasicMapper.insertTStockBasic(tStockBasic);
    }

    /**
     * 修改股票代码
     * 
     * @param tStockBasic 股票代码
     * @return 结果
     */
    @Override
    public int updateTStockBasic(TStockBasic tStockBasic)
    {
        return tStockBasicMapper.updateTStockBasic(tStockBasic);
    }

    /**
     * 删除股票代码对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTStockBasicByIds(String ids)
    {
        return tStockBasicMapper.deleteTStockBasicByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除股票代码信息
     * 
     * @param id 股票代码ID
     * @return 结果
     */
    @Override
    public int deleteTStockBasicById(Long id)
    {
        return tStockBasicMapper.deleteTStockBasicById(id);
    }
}
