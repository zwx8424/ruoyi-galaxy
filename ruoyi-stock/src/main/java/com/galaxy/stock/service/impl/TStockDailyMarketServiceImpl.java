package com.galaxy.stock.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.galaxy.stock.mapper.TStockDailyMarketMapper;
import com.galaxy.stock.domain.TStockDailyMarket;
import com.galaxy.stock.service.ITStockDailyMarketService;
import com.ruoyi.common.core.text.Convert;

/**
 * 股票每日行情数据Service业务层处理
 * 
 * @author lucifer
 * @date 2020-09-12
 */
@Service
public class TStockDailyMarketServiceImpl implements ITStockDailyMarketService 
{
    @Autowired
    private TStockDailyMarketMapper tStockDailyMarketMapper;

    /**
     * 查询股票每日行情数据
     * 
     * @param id 股票每日行情数据ID
     * @return 股票每日行情数据
     */
    @Override
    public TStockDailyMarket selectTStockDailyMarketById(Long id)
    {
        return tStockDailyMarketMapper.selectTStockDailyMarketById(id);
    }

    /**
     * 查询股票每日行情数据列表
     * 
     * @param tStockDailyMarket 股票每日行情数据
     * @return 股票每日行情数据
     */
    @Override
    public List<TStockDailyMarket> selectTStockDailyMarketList(TStockDailyMarket tStockDailyMarket)
    {
        return tStockDailyMarketMapper.selectTStockDailyMarketList(tStockDailyMarket);
    }

    /**
     * 新增股票每日行情数据
     * 
     * @param tStockDailyMarket 股票每日行情数据
     * @return 结果
     */
    @Override
    public int insertTStockDailyMarket(TStockDailyMarket tStockDailyMarket)
    {
        return tStockDailyMarketMapper.insertTStockDailyMarket(tStockDailyMarket);
    }

    /**
     * 修改股票每日行情数据
     * 
     * @param tStockDailyMarket 股票每日行情数据
     * @return 结果
     */
    @Override
    public int updateTStockDailyMarket(TStockDailyMarket tStockDailyMarket)
    {
        return tStockDailyMarketMapper.updateTStockDailyMarket(tStockDailyMarket);
    }

    /**
     * 删除股票每日行情数据对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTStockDailyMarketByIds(String ids)
    {
        return tStockDailyMarketMapper.deleteTStockDailyMarketByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除股票每日行情数据信息
     * 
     * @param id 股票每日行情数据ID
     * @return 结果
     */
    @Override
    public int deleteTStockDailyMarketById(Long id)
    {
        return tStockDailyMarketMapper.deleteTStockDailyMarketById(id);
    }
}
