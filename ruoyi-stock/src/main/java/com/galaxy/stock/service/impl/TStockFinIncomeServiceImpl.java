package com.galaxy.stock.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.galaxy.stock.mapper.TStockFinIncomeMapper;
import com.galaxy.stock.domain.TStockFinIncome;
import com.galaxy.stock.service.ITStockFinIncomeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 股票财务利润表Service业务层处理
 * 
 * @author lucifer
 * @date 2020-09-12
 */
@Service
public class TStockFinIncomeServiceImpl implements ITStockFinIncomeService 
{
    @Autowired
    private TStockFinIncomeMapper tStockFinIncomeMapper;

    /**
     * 查询股票财务利润表
     * 
     * @param id 股票财务利润表ID
     * @return 股票财务利润表
     */
    @Override
    public TStockFinIncome selectTStockFinIncomeById(Long id)
    {
        return tStockFinIncomeMapper.selectTStockFinIncomeById(id);
    }

    /**
     * 查询股票财务利润表列表
     * 
     * @param tStockFinIncome 股票财务利润表
     * @return 股票财务利润表
     */
    @Override
    public List<TStockFinIncome> selectTStockFinIncomeList(TStockFinIncome tStockFinIncome)
    {
        return tStockFinIncomeMapper.selectTStockFinIncomeList(tStockFinIncome);
    }

    /**
     * 新增股票财务利润表
     * 
     * @param tStockFinIncome 股票财务利润表
     * @return 结果
     */
    @Override
    public int insertTStockFinIncome(TStockFinIncome tStockFinIncome)
    {
        return tStockFinIncomeMapper.insertTStockFinIncome(tStockFinIncome);
    }

    /**
     * 修改股票财务利润表
     * 
     * @param tStockFinIncome 股票财务利润表
     * @return 结果
     */
    @Override
    public int updateTStockFinIncome(TStockFinIncome tStockFinIncome)
    {
        return tStockFinIncomeMapper.updateTStockFinIncome(tStockFinIncome);
    }

    /**
     * 删除股票财务利润表对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTStockFinIncomeByIds(String ids)
    {
        return tStockFinIncomeMapper.deleteTStockFinIncomeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除股票财务利润表信息
     * 
     * @param id 股票财务利润表ID
     * @return 结果
     */
    @Override
    public int deleteTStockFinIncomeById(Long id)
    {
        return tStockFinIncomeMapper.deleteTStockFinIncomeById(id);
    }
}
