package com.galaxy.stock.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.galaxy.stock.mapper.TStockDailyQuotaMapper;
import com.galaxy.stock.domain.TStockDailyQuota;
import com.galaxy.stock.service.ITStockDailyQuotaService;
import com.ruoyi.common.core.text.Convert;

/**
 * 股票每日指标Service业务层处理
 * 
 * @author lucifer
 * @date 2020-09-12
 */
@Service
public class TStockDailyQuotaServiceImpl implements ITStockDailyQuotaService 
{
    @Autowired
    private TStockDailyQuotaMapper tStockDailyQuotaMapper;

    /**
     * 查询股票每日指标
     * 
     * @param id 股票每日指标ID
     * @return 股票每日指标
     */
    @Override
    public TStockDailyQuota selectTStockDailyQuotaById(Long id)
    {
        return tStockDailyQuotaMapper.selectTStockDailyQuotaById(id);
    }

    /**
     * 查询股票每日指标列表
     * 
     * @param tStockDailyQuota 股票每日指标
     * @return 股票每日指标
     */
    @Override
    public List<TStockDailyQuota> selectTStockDailyQuotaList(TStockDailyQuota tStockDailyQuota)
    {
        return tStockDailyQuotaMapper.selectTStockDailyQuotaList(tStockDailyQuota);
    }

    /**
     * 新增股票每日指标
     * 
     * @param tStockDailyQuota 股票每日指标
     * @return 结果
     */
    @Override
    public int insertTStockDailyQuota(TStockDailyQuota tStockDailyQuota)
    {
        return tStockDailyQuotaMapper.insertTStockDailyQuota(tStockDailyQuota);
    }

    /**
     * 修改股票每日指标
     * 
     * @param tStockDailyQuota 股票每日指标
     * @return 结果
     */
    @Override
    public int updateTStockDailyQuota(TStockDailyQuota tStockDailyQuota)
    {
        return tStockDailyQuotaMapper.updateTStockDailyQuota(tStockDailyQuota);
    }

    /**
     * 删除股票每日指标对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTStockDailyQuotaByIds(String ids)
    {
        return tStockDailyQuotaMapper.deleteTStockDailyQuotaByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除股票每日指标信息
     * 
     * @param id 股票每日指标ID
     * @return 结果
     */
    @Override
    public int deleteTStockDailyQuotaById(Long id)
    {
        return tStockDailyQuotaMapper.deleteTStockDailyQuotaById(id);
    }
}
