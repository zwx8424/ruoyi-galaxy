package com.galaxy.stock.service;

import java.util.List;
import com.galaxy.stock.domain.TStockDailyMarket;

/**
 * 股票每日行情数据Service接口
 * 
 * @author lucifer
 * @date 2020-09-12
 */
public interface ITStockDailyMarketService 
{
    /**
     * 查询股票每日行情数据
     * 
     * @param id 股票每日行情数据ID
     * @return 股票每日行情数据
     */
    public TStockDailyMarket selectTStockDailyMarketById(Long id);

    /**
     * 查询股票每日行情数据列表
     * 
     * @param tStockDailyMarket 股票每日行情数据
     * @return 股票每日行情数据集合
     */
    public List<TStockDailyMarket> selectTStockDailyMarketList(TStockDailyMarket tStockDailyMarket);

    /**
     * 新增股票每日行情数据
     * 
     * @param tStockDailyMarket 股票每日行情数据
     * @return 结果
     */
    public int insertTStockDailyMarket(TStockDailyMarket tStockDailyMarket);

    /**
     * 修改股票每日行情数据
     * 
     * @param tStockDailyMarket 股票每日行情数据
     * @return 结果
     */
    public int updateTStockDailyMarket(TStockDailyMarket tStockDailyMarket);

    /**
     * 批量删除股票每日行情数据
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTStockDailyMarketByIds(String ids);

    /**
     * 删除股票每日行情数据信息
     * 
     * @param id 股票每日行情数据ID
     * @return 结果
     */
    public int deleteTStockDailyMarketById(Long id);
}
