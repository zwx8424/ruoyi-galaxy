package com.galaxy.stock.controller;

import java.util.List;

import com.galaxy.stock.domain.TStockDailyMarket;
import com.galaxy.stock.service.ITStockDailyMarketService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 股票报表Controller
 * 
 * @author lucifer
 * @date 2020-09-02
 */
@Controller
@RequestMapping("/stock/stockreport")
public class StockReportController extends BaseController {
    private String prefix = "stock/stockreport";

    @Autowired
    private ITStockDailyMarketService tStockDailyMarketService;

    /**
     * 百度ECharts
     */
    @GetMapping("/echarts")
    public String echarts() {
        return prefix + "/echarts";
    }

    /**
     * 查询股票每日数据列表
     */
    @RequiresPermissions("stock:stockreport:view")
    @PostMapping("/klist")
    @ResponseBody
    public TableDataInfo klist(TStockDailyMarket tStockDailyMarket) {
        if (tStockDailyMarket == null || StringUtils.isBlank(tStockDailyMarket.getTsCode())) {
            return null;
        }
        System.out.println("------------------------start");
        List<TStockDailyMarket> list = tStockDailyMarketService.selectTStockDailyMarketList(tStockDailyMarket);
        System.out.println("------------------------end" + list);
        return getDataTable(list);
    }

}
