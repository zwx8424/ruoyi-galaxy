package com.galaxy.stock.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.galaxy.stock.domain.TStockDailyQuota;
import com.galaxy.stock.service.ITStockDailyQuotaService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 股票每日指标Controller
 * 
 * @author lucifer
 * @date 2020-09-12
 */
@Controller
@RequestMapping("/stock/stockdailyquota")
public class TStockDailyQuotaController extends BaseController
{
    private String prefix = "stock/stockdailyquota";

    @Autowired
    private ITStockDailyQuotaService tStockDailyQuotaService;

    @RequiresPermissions("stock:stockdailyquota:view")
    @GetMapping()
    public String stockdailyquota()
    {
        return prefix + "/stockdailyquota";
    }

    /**
     * 查询股票每日指标列表
     */
    @RequiresPermissions("stock:stockdailyquota:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TStockDailyQuota tStockDailyQuota)
    {
        startPage();
        List<TStockDailyQuota> list = tStockDailyQuotaService.selectTStockDailyQuotaList(tStockDailyQuota);
        return getDataTable(list);
    }

    /**
     * 导出股票每日指标列表
     */
    @RequiresPermissions("stock:stockdailyquota:export")
    @Log(title = "股票每日指标", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TStockDailyQuota tStockDailyQuota)
    {
        List<TStockDailyQuota> list = tStockDailyQuotaService.selectTStockDailyQuotaList(tStockDailyQuota);
        ExcelUtil<TStockDailyQuota> util = new ExcelUtil<TStockDailyQuota>(TStockDailyQuota.class);
        return util.exportExcel(list, "stockdailyquota");
    }

    /**
     * 新增股票每日指标
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存股票每日指标
     */
    @RequiresPermissions("stock:stockdailyquota:add")
    @Log(title = "股票每日指标", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TStockDailyQuota tStockDailyQuota)
    {
        return toAjax(tStockDailyQuotaService.insertTStockDailyQuota(tStockDailyQuota));
    }

    /**
     * 修改股票每日指标
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TStockDailyQuota tStockDailyQuota = tStockDailyQuotaService.selectTStockDailyQuotaById(id);
        mmap.put("tStockDailyQuota", tStockDailyQuota);
        return prefix + "/edit";
    }

    /**
     * 修改保存股票每日指标
     */
    @RequiresPermissions("stock:stockdailyquota:edit")
    @Log(title = "股票每日指标", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TStockDailyQuota tStockDailyQuota)
    {
        return toAjax(tStockDailyQuotaService.updateTStockDailyQuota(tStockDailyQuota));
    }

    /**
     * 删除股票每日指标
     */
    @RequiresPermissions("stock:stockdailyquota:remove")
    @Log(title = "股票每日指标", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tStockDailyQuotaService.deleteTStockDailyQuotaByIds(ids));
    }
}
