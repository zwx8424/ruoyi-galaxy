package com.galaxy.stock.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.galaxy.stock.domain.TTableBackupConfig;
import com.galaxy.stock.service.ITTableBackupConfigService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 备份配置Controller
 * 
 * @author lucifer
 * @date 2020-10-18
 */
@Controller
@RequestMapping("/stock/tablebackupconfig")
public class TTableBackupConfigController extends BaseController
{
    private String prefix = "stock/tablebackupconfig";

    @Autowired
    private ITTableBackupConfigService tTableBackupConfigService;

    @RequiresPermissions("stock:tablebackupconfig:view")
    @GetMapping()
    public String tablebackupconfig()
    {
        return prefix + "/tablebackupconfig";
    }

    /**
     * 查询备份配置列表
     */
    @RequiresPermissions("stock:tablebackupconfig:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TTableBackupConfig tTableBackupConfig)
    {
        startPage();
        List<TTableBackupConfig> list = tTableBackupConfigService.selectTTableBackupConfigList(tTableBackupConfig);
        return getDataTable(list);
    }

    /**
     * 导出备份配置列表
     */
    @RequiresPermissions("stock:tablebackupconfig:export")
    @Log(title = "备份配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TTableBackupConfig tTableBackupConfig)
    {
        List<TTableBackupConfig> list = tTableBackupConfigService.selectTTableBackupConfigList(tTableBackupConfig);
        ExcelUtil<TTableBackupConfig> util = new ExcelUtil<TTableBackupConfig>(TTableBackupConfig.class);
        return util.exportExcel(list, "tablebackupconfig");
    }

    /**
     * 新增备份配置
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存备份配置
     */
    @RequiresPermissions("stock:tablebackupconfig:add")
    @Log(title = "备份配置", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TTableBackupConfig tTableBackupConfig)
    {
        return toAjax(tTableBackupConfigService.insertTTableBackupConfig(tTableBackupConfig));
    }

    /**
     * 修改备份配置
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TTableBackupConfig tTableBackupConfig = tTableBackupConfigService.selectTTableBackupConfigById(id);
        mmap.put("tTableBackupConfig", tTableBackupConfig);
        return prefix + "/edit";
    }

    /**
     * 修改保存备份配置
     */
    @RequiresPermissions("stock:tablebackupconfig:edit")
    @Log(title = "备份配置", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TTableBackupConfig tTableBackupConfig)
    {
        return toAjax(tTableBackupConfigService.updateTTableBackupConfig(tTableBackupConfig));
    }

    /**
     * 删除备份配置
     */
    @RequiresPermissions("stock:tablebackupconfig:remove")
    @Log(title = "备份配置", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tTableBackupConfigService.deleteTTableBackupConfigByIds(ids));
    }
}
