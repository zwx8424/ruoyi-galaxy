package com.galaxy.stock.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.galaxy.stock.domain.TStockDailyMarket;
import com.galaxy.stock.service.ITStockDailyMarketService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 股票每日行情数据Controller
 * 
 * @author lucifer
 * @date 2020-09-12
 */
@Controller
@RequestMapping("/stock/stockdailymarket")
public class TStockDailyMarketController extends BaseController
{
    private String prefix = "stock/stockdailymarket";

    @Autowired
    private ITStockDailyMarketService tStockDailyMarketService;

    @RequiresPermissions("stock:stockdailymarket:view")
    @GetMapping()
    public String stockdailymarket()
    {
        return prefix + "/stockdailymarket";
    }

    /**
     * 查询股票每日行情数据列表
     */
    @RequiresPermissions("stock:stockdailymarket:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TStockDailyMarket tStockDailyMarket)
    {
        startPage();
        List<TStockDailyMarket> list = tStockDailyMarketService.selectTStockDailyMarketList(tStockDailyMarket);
        return getDataTable(list);
    }

    /**
     * 导出股票每日行情数据列表
     */
    @RequiresPermissions("stock:stockdailymarket:export")
    @Log(title = "股票每日行情数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TStockDailyMarket tStockDailyMarket)
    {
        List<TStockDailyMarket> list = tStockDailyMarketService.selectTStockDailyMarketList(tStockDailyMarket);
        ExcelUtil<TStockDailyMarket> util = new ExcelUtil<TStockDailyMarket>(TStockDailyMarket.class);
        return util.exportExcel(list, "stockdailymarket");
    }

    /**
     * 新增股票每日行情数据
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存股票每日行情数据
     */
    @RequiresPermissions("stock:stockdailymarket:add")
    @Log(title = "股票每日行情数据", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TStockDailyMarket tStockDailyMarket)
    {
        return toAjax(tStockDailyMarketService.insertTStockDailyMarket(tStockDailyMarket));
    }

    /**
     * 修改股票每日行情数据
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TStockDailyMarket tStockDailyMarket = tStockDailyMarketService.selectTStockDailyMarketById(id);
        mmap.put("tStockDailyMarket", tStockDailyMarket);
        return prefix + "/edit";
    }

    /**
     * 修改保存股票每日行情数据
     */
    @RequiresPermissions("stock:stockdailymarket:edit")
    @Log(title = "股票每日行情数据", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TStockDailyMarket tStockDailyMarket)
    {
        return toAjax(tStockDailyMarketService.updateTStockDailyMarket(tStockDailyMarket));
    }

    /**
     * 删除股票每日行情数据
     */
    @RequiresPermissions("stock:stockdailymarket:remove")
    @Log(title = "股票每日行情数据", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tStockDailyMarketService.deleteTStockDailyMarketByIds(ids));
    }
}
