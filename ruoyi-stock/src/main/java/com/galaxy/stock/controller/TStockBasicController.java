package com.galaxy.stock.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.galaxy.stock.domain.TStockBasic;
import com.galaxy.stock.service.ITStockBasicService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 股票代码Controller
 * 
 * @author lucifer
 * @date 2020-09-12
 */
@Controller
@RequestMapping("/stock/stockbasic")
public class TStockBasicController extends BaseController
{
    private String prefix = "stock/stockbasic";

    @Autowired
    private ITStockBasicService tStockBasicService;

    @RequiresPermissions("stock:stockbasic:view")
    @GetMapping()
    public String stockbasic()
    {
        return prefix + "/stockbasic";
    }

    /**
     * 查询股票代码列表
     */
    @RequiresPermissions("stock:stockbasic:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TStockBasic tStockBasic)
    {
        startPage();
        List<TStockBasic> list = tStockBasicService.selectTStockBasicList(tStockBasic);
        return getDataTable(list);
    }

    /**
     * 导出股票代码列表
     */
    @RequiresPermissions("stock:stockbasic:export")
    @Log(title = "股票代码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TStockBasic tStockBasic)
    {
        List<TStockBasic> list = tStockBasicService.selectTStockBasicList(tStockBasic);
        ExcelUtil<TStockBasic> util = new ExcelUtil<TStockBasic>(TStockBasic.class);
        return util.exportExcel(list, "stockbasic");
    }

    /**
     * 新增股票代码
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存股票代码
     */
    @RequiresPermissions("stock:stockbasic:add")
    @Log(title = "股票代码", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TStockBasic tStockBasic)
    {
        return toAjax(tStockBasicService.insertTStockBasic(tStockBasic));
    }

    /**
     * 修改股票代码
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TStockBasic tStockBasic = tStockBasicService.selectTStockBasicById(id);
        mmap.put("tStockBasic", tStockBasic);
        return prefix + "/edit";
    }

    /**
     * 修改保存股票代码
     */
    @RequiresPermissions("stock:stockbasic:edit")
    @Log(title = "股票代码", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TStockBasic tStockBasic)
    {
        return toAjax(tStockBasicService.updateTStockBasic(tStockBasic));
    }

    /**
     * 删除股票代码
     */
    @RequiresPermissions("stock:stockbasic:remove")
    @Log(title = "股票代码", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tStockBasicService.deleteTStockBasicByIds(ids));
    }
}
