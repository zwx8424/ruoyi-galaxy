package com.galaxy.stock.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.galaxy.stock.domain.TStockFinIncome;
import com.galaxy.stock.service.ITStockFinIncomeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 股票财务利润表Controller
 * 
 * @author lucifer
 * @date 2020-09-12
 */
@Controller
@RequestMapping("/stock/stockfinincome")
public class TStockFinIncomeController extends BaseController
{
    private String prefix = "stock/stockfinincome";

    @Autowired
    private ITStockFinIncomeService tStockFinIncomeService;

    @RequiresPermissions("stock:stockfinincome:view")
    @GetMapping()
    public String stockfinincome()
    {
        return prefix + "/stockfinincome";
    }

    /**
     * 查询股票财务利润表列表
     */
    @RequiresPermissions("stock:stockfinincome:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TStockFinIncome tStockFinIncome)
    {
        startPage();
        List<TStockFinIncome> list = tStockFinIncomeService.selectTStockFinIncomeList(tStockFinIncome);
        return getDataTable(list);
    }

    /**
     * 导出股票财务利润表列表
     */
    @RequiresPermissions("stock:stockfinincome:export")
    @Log(title = "股票财务利润表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TStockFinIncome tStockFinIncome)
    {
        List<TStockFinIncome> list = tStockFinIncomeService.selectTStockFinIncomeList(tStockFinIncome);
        ExcelUtil<TStockFinIncome> util = new ExcelUtil<TStockFinIncome>(TStockFinIncome.class);
        return util.exportExcel(list, "stockfinincome");
    }

    /**
     * 新增股票财务利润表
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存股票财务利润表
     */
    @RequiresPermissions("stock:stockfinincome:add")
    @Log(title = "股票财务利润表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TStockFinIncome tStockFinIncome)
    {
        return toAjax(tStockFinIncomeService.insertTStockFinIncome(tStockFinIncome));
    }

    /**
     * 修改股票财务利润表
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TStockFinIncome tStockFinIncome = tStockFinIncomeService.selectTStockFinIncomeById(id);
        mmap.put("tStockFinIncome", tStockFinIncome);
        return prefix + "/edit";
    }

    /**
     * 修改保存股票财务利润表
     */
    @RequiresPermissions("stock:stockfinincome:edit")
    @Log(title = "股票财务利润表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TStockFinIncome tStockFinIncome)
    {
        return toAjax(tStockFinIncomeService.updateTStockFinIncome(tStockFinIncome));
    }

    /**
     * 删除股票财务利润表
     */
    @RequiresPermissions("stock:stockfinincome:remove")
    @Log(title = "股票财务利润表", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tStockFinIncomeService.deleteTStockFinIncomeByIds(ids));
    }
}
